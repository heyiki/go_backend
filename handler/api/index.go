package api

import (
	result "go_backend/handler"
	"go_backend/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// 获取轮播图的参数
type BannerListParam struct {
	Position int `json:"type" binding:"required" comment:"轮播类型"`
	Num      int `json:"num" binding:"required"  comment:"轮播数量"`
}

// 轮播列表
func BannerList(c *gin.Context) {
	var param BannerListParam
	err := c.BindJSON(&param)
	if err != nil {
		c.JSON(http.StatusBadRequest, result.RetJson(nil, 500, err.Error()))
		return
	}

	var ListData models.Banner
	list := ListData.ListBanner(param.Position, param.Num)
	c.JSON(http.StatusOK, result.RetJson(list, 200, "OK"))
}
