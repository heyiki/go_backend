package handler

import (

)

type Response struct {
	Code int         `json:"code"` // 错误码
	Msg  string      `json:"msg"`  // 错误描述
	Data interface{} `json:"data"` // 返回数据
}

// 自定义统一响应返回
func RetJson(data interface{}, code int, msg string) Response {
	return Response{
		Code: code,
		Msg:  msg,
		Data: data,
	}
}

