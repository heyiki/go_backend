/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 127.0.0.1:3306
 Source Schema         : go_backend

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 12/07/2021 09:42:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hk_attachment
-- ----------------------------
DROP TABLE IF EXISTS `hk_attachment`;
CREATE TABLE `hk_attachment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NULL DEFAULT 0 COMMENT '分类目录ID',
  `name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '名称',
  `att_dir` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路径',
  `satt_dir` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '压缩路径',
  `att_size` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '大小',
  `att_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型',
  `att_mime` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'mime类型',
  `width` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原宽度',
  `height` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原高度',
  `source` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '图片上传类型 1本地 2七牛云 3OSS 4COS ',
  `module_type` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '图片上传模块类型 1 后台上传 2 用户生成',
  `create_time` int(11) NULL DEFAULT 0 COMMENT '上传时间',
  `is_delete` int(11) UNSIGNED NULL DEFAULT 1 COMMENT '删除 1否  0是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for hk_attachment_category
-- ----------------------------
DROP TABLE IF EXISTS `hk_attachment_category`;
CREATE TABLE `hk_attachment_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT 0 COMMENT '父级ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件分类表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_attachment_category
-- ----------------------------

-- ----------------------------
-- Table structure for hk_banner
-- ----------------------------
DROP TABLE IF EXISTS `hk_banner`;
CREATE TABLE `hk_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片名称',
  `position` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '位置 \r\n1-首页轮播',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否显示',
  `sort` smallint(5) NOT NULL DEFAULT 50 COMMENT '排序',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '地址',
  `is_del` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '软删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '轮播图表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_banner
-- ----------------------------
INSERT INTO `hk_banner` VALUES (1, 'http://xxxx/uploads/banner/20210611\\2fc6775d6861e0eb019d324b324fae57.jpg', '123', 1, 1, 50, '', 0);

-- ----------------------------
-- Table structure for hk_test
-- ----------------------------
DROP TABLE IF EXISTS `hk_test`;
CREATE TABLE `hk_test`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配置字段键值对',
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置键值',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `update_time` int(11) NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hk_test
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
