package main

import (
	"fmt"
	"log"
	"net/http"
	"path"
	"strconv"

	"github.com/gin-gonic/gin"
)

// map字典
func mapTest() {
	// 定义 变量strMap
	var strMap map[int]string
	// 进行初始化
	strMap = make(map[int]string)

	// 给map 赋值
	for i := 0; i < 5; i++ {
		strMap[i] = "测试"
	}

	// 打印出map值
	for k, v := range strMap {
		fmt.Println(k, ":", v)
	}

	// 打印出map 长度
	fmt.Println(len(strMap))
}

// 切片
func section() {
	// 方式一
	// a := make([]int,5) // 初始化长度为5的slice,默认值为零
	// for i := 0; i <5; i++ {
	//    a = append(a, i)
	// }
	// a = append(a, 6)
	// fmt.Println(a) // [0 0 0 0 0 0 1 2 3 4 6]

	// 方式二
	var a []int
	for i := 0; i < 5; i++ {
		a = append(a, i)
	}
	fmt.Println(a) // [0 1 2 3 4]
}

// 字符串
func stringTest() {
	var str string
	str = "你好你好"
	fmt.Println(str)
}

// 数组
func arrayTest() {
	var arr1 [4]int   // 元素自动初始化为零
	fmt.Println(arr1) // [0 0 0 0]

	arr2 := [4]int{1, 2} // 其他未初始化的元素为零
	fmt.Println(arr2)    // [1 2 0 0]

	arr3 := [4]int{5, 3: 10} // 可指定索引位置初始化
	fmt.Println(arr3)        // [5 0 0 10]

	arr4 := [...]int{1, 2, 3} // 编译器按照初始化值数量确定数组长度
	fmt.Println(arr4)         // [1 2 3]

	t := len(arr4) // 内置函数len(数组名称)表示数组的长度
	fmt.Println(t) // 3
}

// 判断语句
func judge() {
	x := 3
	if x > 5 {
		fmt.Println("大于5")
	} else if x < 5 && x > 3 {
		fmt.Println("大于3小于5")
	} else {
		fmt.Println("小于3")
	}
}

// 循环语句
func loop() {
	for i := 0; i < 5; i++ {
		if i == 4 {
			continue
		} else if i == 5 {
			break
		}
		fmt.Println(i)
	}
}

func main() { // main函数，是程序执行的入口
	// fmt.Println("Hello World!")  // 在终端打印 Hello World!

	// mapTest()
	// section()
	// stringTest()
	// arrayTest()
	// judge()
	// loop()

	// a := []int{1, 2, 4, 5, 67, 84, 324}
	// a = append(a, 2)
	// sub := a[1:len(a)]
	// fmt.Println(a)
	// fmt.Println(sub)

	// var a []int
	// fmt.Println(append(a,2))
	// fmt.Printf("array:%v",append(a,2))
	// fmt.Printf("%.1f",10.23665)

    // html := `<img src="xxx.png"/>`
	// test := `<a href="%s">`+html+`</a>`
	// fmt.Sprintf(test,"sad")
	// fmt.Println(test)

	// 获取路由
	router := gin.Default()

	// 最简单的回应 http://127.0.0.1:8080/ping
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"msg": "pong(最简单的回应)",
		})
	})

	// GET版 鹦鹉学舌 http://127.0.0.1:8080/message?name=dd
	router.GET("/message", func(c *gin.Context) {
		name := c.Query("name")
		data := map[string]interface{}{
			"name": name,
		}
		c.JSON(200, gin.H{
			"code": 1000,
			"msg":  "响应鹦鹉学舌",
			"data": data,
		})
	})

	// POST版鹦鹉学舌 http://127.0.0.1:8080/movie

	type Info struct {
		Name  string `json:"name"`
		Score int    `json:"score"`
	}

	router.POST("/movie", func(c *gin.Context) {

		// 以Info为模板初始化data
		var data Info

		// 将请求参数绑定到data
		// c.BindJSON(&data);
		// 接收错误
		if err := c.BindJSON(&data); err != nil {
			fmt.Println("err", err)
		}

		fmt.Println("=data=>>", data)

		c.JSON(200, gin.H{
			"code": 1000,
			"msg":  "返回电影名称和评分",
			"data": data,
		})
		return
	})

	router.POST("/movie_form", func(c *gin.Context) {

		name := c.PostForm("name")
		score := c.PostForm("score")

		fmt.Println("=name=>>", name)
		fmt.Println("=score=>>", score)

		var data Info

		scoreInt, _ := strconv.Atoi(score)

		data.Name = name
		data.Score = scoreInt

		c.JSON(200, gin.H{
			"code": 1000,
			"msg":  "返回电影名称和评分",
			"data": data,
		})
		return
	})

	// 为 multipart forms 设置较低的内存限制 (默认是 32 MiB)
	router.MaxMultipartMemory = 8 << 20 // 8 MiB
	router.POST("/upload", func(c *gin.Context) {
		// 单文件
		file, _ := c.FormFile("file")
		log.Println(file.Filename)

		// 设置文件需要保存的指定位置并设置保存的文件名字
		dst := path.Join("./upload", file.Filename)
		// 自定义文件名称，需要带上文件的格式后缀
		// dst := path.Join("./upload", "tupian.jpg")

		// 上传文件至指定目录
		c.SaveUploadedFile(file, dst)

		c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))

		return
	})

	router.Run() // 监听并在 0.0.0.0:8080 上启动服务

}
