module go_backend

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.2
	github.com/go-sql-driver/mysql v1.6.0
	github.com/go-xorm/xorm v0.7.9
	github.com/kr/pretty v0.2.1 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
