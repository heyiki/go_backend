# Golang Practice2

#### 介绍
go语言练习，gin框架+xorm数据库管理，主要执行原生sql语句

#### 本项目文件说明

> Project 项目文件

> -- handler 控制器文件夹

> -- middleware 中间间文件夹

> -- models 模型文件夹

> -- utils 工具类文件夹

> main.go 入口文件

> go.sum 记录所依赖的项目版本的锁定

> go.mod 包管理，定义其所在的目录是一个模块

#### 部署

如果没有go.mod，在根目录执行`go mod init`，初始化go.mod，然后执行`go mod tidy`，根据 go.mod 文件来处理依赖关系

go_backend.sql数据库导入

不出意外，执行`go run main.go`可以正常执行