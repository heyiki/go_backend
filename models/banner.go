package models

import (
	"fmt"
	"log"
)

type Banner struct {
	Id       int64  `json:"id"`
	Picture  string `json:"picture"`
	Name     string `json:"name"`
	Url      string `json:"url"`
	Position int    `json:"position"`
}

// 轮播列表
func (a *Banner) ListBanner(position int, num int) []Banner {

	var list []Banner

	sql := fmt.Sprintf(`select id,picture,name,url,position from hk_banner where position = %d order by sort desc limit %d`, position, num)

	if err := engine.SQL(sql).Find(&list); err != nil {
		log.Println("sql：", err)
	}

	return list
}
