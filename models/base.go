package models

import (
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
)

var engine *xorm.Engine // 定义orm引擎
var err error

// 测试表结构体
// type Test struct {
// 	Id        int32  `xorm:"int(11) autoincr pk notnull comment('主键id')"`
// 	Name      string `xorm:"varchar(50) comment('配置字段键值对')"`
// 	Value     string `xorm:"longtext comment('配置键值')"`
// 	Type      string `xorm:"varchar(32) comment('类型')"`
// 	UpdatedAt int32  `xorm:"int(11) 'update_time' default(0) comment('更新时间')"`
// }

func init() {
	// 数据库连接参数
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=true", "root", "root", "localhost:3306", "go_backend") //账号 密码 连接地址:端口 数据库名称

	engine, err = xorm.NewEngine("mysql", dsn)

	if err != nil {
		log.Println("Database connection failed :", err) // 数据库连接失败
	}

	// engine.ShowSQL(true) // 打印sql语句

	// // 添加统一前缀
	// tbMapper := core.NewPrefixMapper(core.SnakeMapper{}, "hk_")
	// engine.SetTableMapper(tbMapper)
	// defer engine.Close()

	// // 创建表==测试创建，其他直接在mysql创建，结构体创建表不熟练，其他类型不知道怎么写
	// if err := engine.Sync2(new(Test)); err != nil {
	// 	log.Fatal("Data table synchronization failed:", err)
	// }
}
