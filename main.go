package main

import (
	"go_backend/handler/api"
	_ "go_backend/models"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	engine := gin.Default()

	apiGroup := engine.Group("/api")
	{
		apiGroup.POST("/index/banner", api.BannerList)
	}

	// engine.GET("/test", func(c *gin.Context) {
	// 	res := struct {
	// 		Name  string `json:"name"`
	// 		Age   int    `json:"age"`
	// 		Email string `json:"email"`
	// 	}{
	// 		Name:  "xx",
	// 		Age:   11,
	// 		Email: "xxx@qq.com",
	// 	}
	// 	c.JSON(http.StatusOK, result.RetJson(res, 200, "OK"))
	// 	// c.JSON(http.StatusInternalServerError, result.RetJson(res,200,"OK"))
	// })

	engine.POST("/upload", func(c *gin.Context) {
		// 单文件
		file, _ := c.FormFile("file")
		log.Println(file.Filename)

		// 上传文件到指定的路径
		var dst = "123.jpg"
		c.SaveUploadedFile(file, dst)

		c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
	})

	if err := engine.Run(":8080"); err != nil {
		log.Fatal(err)
	}
}
